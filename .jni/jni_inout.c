/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#ifndef _jni_inout_c
#define _jni_inout_c

#include "jni_inout.h"

// Some programming examples in https://gist.github.com/bitsnaps/3cfdcf92f6dfef952fbfc5f50c4713b6

JNIEnv *javaEnv = NULL;

////////////////////////////////////////////////////
//                     Integer                    //
////////////////////////////////////////////////////
jobject newIntegerObject
  (int* value)
{
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Ljava/lang/Integer;");
	// For a Java class, the field descriptor is in the form
	// of "L<fully-qualified-name>;", with dot replaced by forward
	// slash (/), e.g.,, the class descriptor for Integer is
	// "Ljava/lang/Integer;". For primitives, use "I" for int,
	// "B" for byte, "S" for short, "J" for long, "F" for float,
	// "D" for double, "C" for char, and "Z" for boolean.

	if (value == NULL) {
		return NULL;
	} else {
		jmethodID constructor = (*javaEnv)->GetMethodID(javaEnv, cls, "<init>", "(I)V");
		return (*javaEnv)->NewObject(javaEnv, cls, constructor, *value);
	}
}

void setIntegerValue
  (jobject object, int* value)
{
	// Set value of the instance variable "value" of primitive type "I"
    if (! (*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
		jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
		jfieldID field = (*javaEnv)->GetFieldID(javaEnv, cls, "value", "I");
		if (value == NULL) {
			(*javaEnv)->SetIntField(javaEnv, object, field, 0);
		} else {
			(*javaEnv)->SetIntField(javaEnv, object, field, *value);
		}
    }
}

int getIntegerValue
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
    	return 0;
    } else {
		// Get value of the instance variable "value" of primitive type "I"
		jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
		jfieldID field = (*javaEnv)->GetFieldID(javaEnv, cls, "value", "I");
		return (*javaEnv)->GetIntField(javaEnv, object, field);
    }
}

int* intPtr
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
    	return NULL;
    } else {
    	int* ptr = (int*) malloc(sizeof(int));
    	*ptr = getIntegerValue(object);
    	return ptr;
    }
}

void setIntPtrValue
  (int* intPtr, int value)
{
	if (intPtr != NULL) {
		*intPtr = value;
	}
}

int getIntPtrValue
  (int* intPtr)
{
	if (intPtr == NULL) {
		return 0;
	} else {
		return *intPtr;
	}
}

int** toIntMatrixPtr
  (int* matrix, int rows, int columns)
{
	int** matrixPtr = (int**) malloc(rows * sizeof(int*));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
    	matrixPtr[row] = (int*) malloc(columns * sizeof(int));
	    for (int column = 0; column < columns; column++ ) {
	    	matrixPtr[row][column] = matrix[index++];
	    }
    }
	return matrixPtr;
}

int* toIntMatrixRegionPtr
  (int** matrix, int rows, int columns)
{
	int* matrixRegionPtr = (int*) malloc(rows * columns * sizeof(int));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
	    for (int column = 0; column < columns; column++ ) {
	    	matrixRegionPtr[index++] = matrix[row][column];
	    }
    }
	return matrixRegionPtr;
}

void setIntArray
  (int* outArray, jintArray arrayValue, jint arrayLength)
{
	if (outArray != NULL && arrayLength > 0) {
		jint* inArrayValue = (*javaEnv)->GetIntArrayElements(javaEnv, arrayValue, NULL);
		int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
		int length = arrayLength < inArrayLength ? arrayLength : inArrayLength;
		for(int k=0; k < length; k++){
			outArray[k] = (int)inArrayValue[k];
		}
		for(int k=length; k < arrayLength; k++){
			outArray[k] = 0;
		}
	}
}

void setIntMatrix
  (int** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jintArray inRow;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jintArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				inMatrixValue[k] = toIntArray(inRow, matrixColumns);
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jintArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				inMatrixValue[k] = toIntArray(inRow, matrixColumns);
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				inMatrixValue[k] = (int*) malloc(matrixColumns * sizeof(int));
			}
		}
	}
}

void setIntMatrixRegion
  (int* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jintArray inRow;
		int index = 0;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jintArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setIntArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jintArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setIntArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				for (int h = 0; h < matrixColumns; h++) {
					inMatrixValue[index] = 0;
					index++;
				}
			}
		}
	}
}

int toInt
  (jint value)
{
	return (int) value;
}

int* toIntArray
  (jintArray arrayValue, jint arrayLength)
{
	int* outArray = (int*)calloc(sizeof(int), arrayLength);
	jint* inArrayValue = (*javaEnv)->GetIntArrayElements(javaEnv, arrayValue, NULL);
	int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
	int length = arrayLength < inArrayLength ? arrayLength : inArrayLength;
	for(int k=0; k < length; k++){
		outArray[k] = (int)inArrayValue[k];
	}
	for(int k=length; k < arrayLength; k++){
		outArray[k] = 0;
	}
	return outArray;
}

int** toIntMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    int** inMatrixValue = (int**) malloc(matrixRows * sizeof(int*));
    int   inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
	jintArray inRow;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jintArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			inMatrixValue[k] = toIntArray(inRow, matrixColumns);
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jintArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			inMatrixValue[k] = toIntArray(inRow, matrixColumns);
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			inMatrixValue[k] = (int*) malloc(matrixColumns * sizeof(int));
		}
    }
	return inMatrixValue;
}

int* toIntMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    int* inMatrixValue = (int*) malloc(matrixRows * matrixColumns * sizeof(int));
    int  inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
	jintArray inRow;
	int index = 0;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jintArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setIntArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jintArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setIntArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			for (int h = 0; h < matrixColumns; h++) {
				inMatrixValue[index] = 0;
				index++;
			}
		}
    }
	return inMatrixValue;
}

void setJintArray
  (jintArray outArray, int* arrayValue, int* arrayLength)
{
	if (arrayValue != NULL && arrayValue != NULL && arrayLength != NULL) {
		jint* jarrayValue = (jint*)calloc(sizeof(jint), *arrayLength);
		for(int k=0; k < *arrayLength; k++){
			jarrayValue[k] = (jint) arrayValue[k];
		}
		int outArrayLength = (*javaEnv)->GetArrayLength(javaEnv, outArray);
		if (outArrayLength < *arrayLength) {
			(*javaEnv)->SetIntArrayRegion(javaEnv, outArray, 0, outArrayLength, jarrayValue);
		} else {
			(*javaEnv)->SetIntArrayRegion(javaEnv, outArray, 0, *arrayLength, jarrayValue);
		}
	}
}

void setJintMatrix
  (jobjectArray matrix, int** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL) {
		jintArray rowValue;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJintArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJintArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		}
	}
}

void setJintMatrixRegion
  (jobjectArray matrix, int* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL && matrixColumns != NULL) {
		jintArray rowValue;
		int index = 0;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJintArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJintArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		}
	}
}

jint toJint
  (int value)
{
	return (jint) value;
}

jintArray toJintArray
  (int* arrayValue, int* arrayLength)
{
	if (arrayValue == NULL || arrayLength == NULL) {
		return (*javaEnv)->NewIntArray(javaEnv, 0);
	} else {
		jint* jarrayValue = (jint*)calloc(sizeof(jint), *arrayLength);
		for(int k=0; k < *arrayLength; k++){
			jarrayValue[k] = (jint) arrayValue[k];
		}
		jintArray outArray = (*javaEnv)->NewIntArray(javaEnv, *arrayLength);
		(*javaEnv)->SetIntArrayRegion(javaEnv, outArray, 0, *arrayLength, jarrayValue);
		return outArray;
	}
}

jobjectArray toJintMatrix
  (int** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv,"[I"), NULL);
	} else {
		jintArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv,"[I"), NULL);
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJintArray(matrixValue[k], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
		}
		return matrix;
	}
}

jobjectArray toJintMatrixRegion
  (int* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL || matrixColumns == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv,"[I"), NULL);
	} else {
		jintArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv,"[I"), NULL);
		int index = 0;
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJintArray(&matrixValue[index], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			index += *matrixColumns;
		}
		return matrix;
	}
}


////////////////////////////////////////////////////
//                      Byte                      //
////////////////////////////////////////////////////
jobject newByteObject
  (byte* value)
{
	if (value == NULL) {
		return NULL;
	} else {
		jclass cls = (*javaEnv)->FindClass(javaEnv, "Ljava/lang/Byte;");
		jmethodID constructor = (*javaEnv)->GetMethodID(javaEnv, cls, "<init>", "(B)V");
		return (*javaEnv)->NewObject(javaEnv, cls, constructor, *value);
	}
}

void setByteValue
  (jobject object, byte* value)
{
    if (! (*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
		jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
		jfieldID field = (*javaEnv)->GetFieldID(javaEnv, cls, "value", "B");
		if (value == NULL) {
			(*javaEnv)->SetByteField(javaEnv, object, field, 0);
		} else {
			(*javaEnv)->SetByteField(javaEnv, object, field, *value);
		}
    }
}

byte getByteValue
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
    	return 0;
    } else {
		jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
		jfieldID field = (*javaEnv)->GetFieldID(javaEnv, cls, "value", "B");
		return (*javaEnv)->GetByteField(javaEnv, object, field);
    }
}

byte* bytePtr
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
    	return NULL;
    } else {
    	byte* ptr = (byte*) malloc(sizeof(byte));
    	*ptr = getByteValue(object);
    	return ptr;
    }
}

void setBytePtrValue
  (byte* bytePtr, byte value)
{
	if (bytePtr != NULL) {
		*bytePtr = value;
	}
}

byte getBytePtrValue
  (byte* bytePtr)
{
	if (bytePtr == NULL) {
		return 0;
	} else {
		return *bytePtr;
	}
}

byte** toByteMatrixPtr
  (byte* matrix, int rows, int columns)
{
	byte** matrixPtr = (byte**) malloc(rows * sizeof(byte*));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
    	matrixPtr[row] = (byte*) malloc(columns * sizeof(byte));
	    for (int column = 0; column < columns; column++ ) {
	    	matrixPtr[row][column] = matrix[index++];
	    }
    }
	return matrixPtr;
}

byte* toByteMatrixRegionPtr
  (byte** matrix, int rows, int columns)
{
	byte* matrixRegionPtr = (byte*) malloc(rows * columns * sizeof(byte));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
	    for (int column = 0; column < columns; column++ ) {
	    	matrixRegionPtr[index++] = matrix[row][column];
	    }
    }
	return matrixRegionPtr;
}

void setByteArray
  (byte* outArray, jbyteArray arrayValue, jint arrayLength)
{
	if (outArray != NULL && arrayLength > 0) {
		jbyte* inArrayValue = (*javaEnv)->GetByteArrayElements(javaEnv, arrayValue, NULL);
		int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
		int length = arrayLength < inArrayLength ? arrayLength : inArrayLength;
		for(int k=0; k < length; k++){
			outArray[k] = (byte)inArrayValue[k];
		}
		for(int k=length; k < arrayLength; k++){
			outArray[k] = 0;
		}
	}
}

void setByteMatrix
  (byte** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jbyteArray inRow;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jbyteArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				inMatrixValue[k] = toByteArray(inRow, matrixColumns);
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jbyteArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				inMatrixValue[k] = toByteArray(inRow, matrixColumns);
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				inMatrixValue[k] = (byte*) malloc(matrixColumns * sizeof(byte));
			}
		}
	}
}

void setByteMatrixRegion
  (byte* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jbyteArray inRow;
		int index = 0;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jbyteArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setByteArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jbyteArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setByteArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				for (int h = 0; h < matrixColumns; h++) {
					inMatrixValue[index] = 0;
					index++;
				}
			}
		}
	}
}

byte toByte
  (jbyte value)
{
	return (byte) value;
}

byte* toByteArray
  (jbyteArray arrayValue, jint arrayLength)
{
	byte* outArray = (byte*)calloc(sizeof(byte), arrayLength);
	jbyte* inArrayValue = (*javaEnv)->GetByteArrayElements(javaEnv, arrayValue, NULL);
	int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
	int length = arrayLength < inArrayLength ? arrayLength : inArrayLength;
	for(int k=0; k < length; k++){
		outArray[k] = (byte)inArrayValue[k];
	}
	for(int k=length; k < arrayLength; k++){
		outArray[k] = 0;
	}
	return outArray;
}

byte** toByteMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	byte** inMatrixValue = (byte**) malloc(matrixRows * sizeof(byte*));
    int      inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
	jbyteArray inRow;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jbyteArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			inMatrixValue[k] = toByteArray(inRow, matrixColumns);
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jbyteArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			inMatrixValue[k] = toByteArray(inRow, matrixColumns);
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			inMatrixValue[k] = (byte*) malloc(matrixColumns * sizeof(byte));
		}
    }
	return inMatrixValue;
}

byte* toByteMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    byte* inMatrixValue = (byte*) malloc(matrixRows * matrixColumns * sizeof(byte*));
    int  inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
	jbyteArray inRow;
	int index = 0;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jbyteArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setByteArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jbyteArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setByteArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			for (int h = 0; h < matrixColumns; h++) {
				inMatrixValue[index] = 0;
				index++;
			}
		}
    }
	return inMatrixValue;
}

void setJbyteArray
  (jbyteArray outArray, byte* arrayValue, int* arrayLength)
{
	if (arrayValue != NULL && arrayValue != NULL && arrayLength != NULL) {
		jbyte* jarrayValue = (jbyte*)calloc(sizeof(jbyte), *arrayLength);
		for(int k=0; k < *arrayLength; k++){
			jarrayValue[k] = (jbyte) arrayValue[k];
		}
		int outArrayLength = (*javaEnv)->GetArrayLength(javaEnv, outArray);
		if (outArrayLength < *arrayLength) {
			(*javaEnv)->SetByteArrayRegion(javaEnv, outArray, 0, outArrayLength, jarrayValue);
		} else {
			(*javaEnv)->SetByteArrayRegion(javaEnv, outArray, 0, *arrayLength, jarrayValue);
		}
	}
}

void setJbyteMatrix
  (jobjectArray matrix, byte** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL) {
		jbyteArray rowValue;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJbyteArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJbyteArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		}
	}
}

void setJbyteMatrixRegion
  (jobjectArray matrix, byte* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL && matrixColumns != NULL) {
		jbyteArray rowValue;
		int index = 0;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJbyteArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJbyteArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		}
	}
}

jbyte toJbyte
  (byte value)
{
	return (jbyte) value;
}

jbyteArray toJbyteArray
  (byte* arrayValue, int* arrayLength)
{
	if (arrayValue == NULL || arrayLength == NULL) {
		return (*javaEnv)->NewByteArray(javaEnv, 0);
	} else {
		jbyte* jarrayValue = (jbyte*)calloc(sizeof(jbyte), *arrayLength);
		for(int k=0; k < *arrayLength; k++){
			jarrayValue[k] = (jbyte) arrayValue[k];
		}
		jbyteArray outArray = (*javaEnv)->NewByteArray(javaEnv, *arrayLength);
		(*javaEnv)->SetByteArrayRegion(javaEnv, outArray, 0, *arrayLength, jarrayValue);
		return outArray;
	}
}

jobjectArray toJbyteMatrix
  (byte** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv,"[B"), NULL);
	} else {
		jbyteArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv,"[B"), NULL);
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJbyteArray(matrixValue[k], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
		}
		return matrix;
	}
}

jobjectArray toJbyteMatrixRegion
  (byte* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL || matrixColumns == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv,"[B"), NULL);
	} else {
		jbyteArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv,"[B"), NULL);
		int index = 0;
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJbyteArray(&matrixValue[index], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			index += *matrixColumns;
		}
		return matrix;
	}
}


////////////////////////////////////////////////////
//                     Short                     //
////////////////////////////////////////////////////
jobject newShortObject
  (short* value)
{
	if (value == NULL) {
		return NULL;
	} else {
		jclass cls = (*javaEnv)->FindClass(javaEnv, "Ljava/lang/Short;");
		jmethodID constructor = (*javaEnv)->GetMethodID(javaEnv, cls, "<init>", "(S)V");
		return (*javaEnv)->NewObject(javaEnv, cls, constructor, *value);
	}
}

void setShortValue
  (jobject object, short* value)
{
    if (! (*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
		jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
		jfieldID field = (*javaEnv)->GetFieldID(javaEnv, cls, "value", "S");
		if (value == NULL) {
			(*javaEnv)->SetShortField(javaEnv, object, field, 0);
		} else {
			(*javaEnv)->SetShortField(javaEnv, object, field, *value);
		}
    }
}

short getShortValue
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
    	return 0;
    } else {
		jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
		jfieldID field = (*javaEnv)->GetFieldID(javaEnv, cls, "value", "S");
		return (*javaEnv)->GetShortField(javaEnv, object, field);
    }
}

short* shortPtr
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
    	return NULL;
    } else {
    	short* ptr = (short*) malloc(sizeof(short));
    	*ptr = getShortValue(object);
    	return ptr;
    }
}

void setShortPtrValue
  (short* shortPtr, short value)
{
	if (shortPtr != NULL) {
		*shortPtr = value;
	}
}

short getShortPtrValue
  (short* shortPtr)
{
	if (shortPtr == NULL) {
		return 0;
	} else {
		return *shortPtr;
	}
}

short** toShortMatrixPtr
  (short* matrix, int rows, int columns)
{
	short** matrixPtr = (short**) malloc(rows * sizeof(short*));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
    	matrixPtr[row] = (short*) malloc(columns * sizeof(short));
	    for (int column = 0; column < columns; column++ ) {
	    	matrixPtr[row][column] = matrix[index++];
	    }
    }
	return matrixPtr;
}

short* toShortMatrixRegionPtr
  (short** matrix, int rows, int columns)
{
	short* matrixRegionPtr = (short*) malloc(rows * columns * sizeof(short));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
	    for (int column = 0; column < columns; column++ ) {
	    	matrixRegionPtr[index++] = matrix[row][column];
	    }
    }
	return matrixRegionPtr;
}

void setShortArray
  (short* outArray, jshortArray arrayValue, jint arrayLength)
{
	if (outArray != NULL && arrayLength > 0) {
		jshort* inArrayValue = (*javaEnv)->GetShortArrayElements(javaEnv, arrayValue, NULL);
		int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
		int length = arrayLength < inArrayLength ? arrayLength : inArrayLength;
		for(int k=0; k < length; k++){
			outArray[k] = (short)inArrayValue[k];
		}
		for(int k=length; k < arrayLength; k++){
			outArray[k] = 0;
		}
	}
}

void setShortMatrix
  (short** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jshortArray inRow;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jshortArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				inMatrixValue[k] = toShortArray(inRow, matrixColumns);
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jshortArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				inMatrixValue[k] = toShortArray(inRow, matrixColumns);
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				inMatrixValue[k] = (short*) malloc(matrixColumns * sizeof(short));
			}
		}
	}
}

void setShortMatrixRegion
  (short* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jshortArray inRow;
		int index = 0;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jshortArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setShortArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jshortArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setShortArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				for (int h = 0; h < matrixColumns; h++) {
					inMatrixValue[index] = 0;
					index++;
				}
			}
		}
	}
}

short toShort
  (jshort value)
{
	return (short) value;
}

short* toShortArray
  (jshortArray arrayValue, jint arrayLength)
{
	short* outArray = (short*)calloc(sizeof(short), arrayLength);
	jshort* inArrayValue = (*javaEnv)->GetShortArrayElements(javaEnv, arrayValue, NULL);
	int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
	int length = arrayLength < inArrayLength ? arrayLength : inArrayLength;
	for(int k=0; k < length; k++){
		outArray[k] = (short)inArrayValue[k];
	}
	for(int k=length; k < arrayLength; k++){
		outArray[k] = 0;
	}
	return outArray;
}

short** toShortMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	short** inMatrixValue = (short**) malloc(matrixRows * sizeof(short*));
    int      inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
	jshortArray inRow;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jshortArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			inMatrixValue[k] = toShortArray(inRow, matrixColumns);
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jshortArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			inMatrixValue[k] = toShortArray(inRow, matrixColumns);
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			inMatrixValue[k] = (short*) malloc(matrixColumns * sizeof(short));
		}
    }
	return inMatrixValue;
}

short* toShortMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    short* inMatrixValue = (short*) malloc(matrixRows * matrixColumns * sizeof(short*));
    int  inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
	jshortArray inRow;
	int index = 0;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jshortArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setShortArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jshortArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setShortArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			for (int h = 0; h < matrixColumns; h++) {
				inMatrixValue[index] = 0;
				index++;
			}
		}
    }
	return inMatrixValue;
}

void setJshortArray
  (jshortArray outArray, short* arrayValue, int* arrayLength)
{
	if (arrayValue != NULL && arrayValue != NULL && arrayLength != NULL) {
		jshort* jarrayValue = (jshort*)calloc(sizeof(jshort), *arrayLength);
		for(int k=0; k < *arrayLength; k++){
			jarrayValue[k] = (jshort) arrayValue[k];
		}
		int outArrayLength = (*javaEnv)->GetArrayLength(javaEnv, outArray);
		if (outArrayLength < *arrayLength) {
			(*javaEnv)->SetShortArrayRegion(javaEnv, outArray, 0, outArrayLength, jarrayValue);
		} else {
			(*javaEnv)->SetShortArrayRegion(javaEnv, outArray, 0, *arrayLength, jarrayValue);
		}
	}
}

void setJshortMatrix
  (jobjectArray matrix, short** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL) {
		jshortArray rowValue;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJshortArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJshortArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		}
	}
}

void setJshortMatrixRegion
  (jobjectArray matrix, short* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL && matrixColumns != NULL) {
		jshortArray rowValue;
		int index = 0;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJshortArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJshortArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		}
	}
}

jshort toJshort
  (short value)
{
	return (jshort) value;
}

jshortArray toJshortArray
  (short* arrayValue, int* arrayLength)
{
	if (arrayValue == NULL || arrayLength == NULL) {
		return (*javaEnv)->NewShortArray(javaEnv, 0);
	} else {
		jshort* jarrayValue = (jshort*)calloc(sizeof(jshort), *arrayLength);
		for(int k=0; k < *arrayLength; k++){
			jarrayValue[k] = (jshort) arrayValue[k];
		}
		jshortArray outArray = (*javaEnv)->NewShortArray(javaEnv, *arrayLength);
		(*javaEnv)->SetShortArrayRegion(javaEnv, outArray, 0, *arrayLength, jarrayValue);
		return outArray;
	}
}

jobjectArray toJshortMatrix
  (short** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv,"[S"), NULL);
	} else {
		jshortArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv,"[S"), NULL);
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJshortArray(matrixValue[k], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
		}
		return matrix;
	}
}

jobjectArray toJshortMatrixRegion
  (short* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL || matrixColumns == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv,"[S"), NULL);
	} else {
		jshortArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv,"[S"), NULL);
		int index = 0;
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJshortArray(&matrixValue[index], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			index += *matrixColumns;
		}
		return matrix;
	}
}


////////////////////////////////////////////////////
//                     Long                     //
////////////////////////////////////////////////////
jobject newLongObject
  (long* value)
{
	if (value == NULL) {
		return NULL;
	} else {
		jclass cls = (*javaEnv)->FindClass(javaEnv, "Ljava/lang/Long;");
		jmethodID constructor = (*javaEnv)->GetMethodID(javaEnv, cls, "<init>", "(J)V");
		return (*javaEnv)->NewObject(javaEnv, cls, constructor, *value);
	}
}

void setLongValue
  (jobject object, long* value)
{
    if (! (*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
		jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
		jfieldID field = (*javaEnv)->GetFieldID(javaEnv, cls, "value", "J");
		if (value == NULL) {
			(*javaEnv)->SetLongField(javaEnv, object, field, 0);
		} else {
			(*javaEnv)->SetLongField(javaEnv, object, field, *value);
		}
    }
}

long getLongValue
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
    	return 0;
    } else {
		jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
		jfieldID field = (*javaEnv)->GetFieldID(javaEnv, cls, "value", "J");
		return (*javaEnv)->GetLongField(javaEnv, object, field);
    }
}

long* longPtr
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
    	return NULL;
    } else {
    	long* ptr = (long*) malloc(sizeof(long));
    	*ptr = getLongValue(object);
    	return ptr;
    }
}

void setLongPtrValue
  (long* longPtr, long value)
{
	if (longPtr != NULL) {
		*longPtr = value;
	}
}

long getLongPtrValue
  (long* longPtr)
{
	if (longPtr == NULL) {
		return 0;
	} else {
		return *longPtr;
	}
}

long** toLongMatrixPtr
  (long* matrix, int rows, int columns)
{
	long** matrixPtr = (long**) malloc(rows * sizeof(long*));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
    	matrixPtr[row] = (long*) malloc(columns * sizeof(long));
	    for (int column = 0; column < columns; column++ ) {
	    	matrixPtr[row][column] = matrix[index++];
	    }
    }
	return matrixPtr;
}

long* toLongMatrixRegionPtr
  (long** matrix, int rows, int columns)
{
	long* matrixRegionPtr = (long*) malloc(rows * columns * sizeof(long));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
	    for (int column = 0; column < columns; column++ ) {
	    	matrixRegionPtr[index++] = matrix[row][column];
	    }
    }
	return matrixRegionPtr;
}

void setLongArray
  (long* outArray, jlongArray arrayValue, jint arrayLength)
{
	if (outArray != NULL && arrayLength > 0) {
		jlong* inArrayValue = (*javaEnv)->GetLongArrayElements(javaEnv, arrayValue, NULL);
		int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
		int length = arrayLength < inArrayLength ? arrayLength : inArrayLength;
		for(int k=0; k < length; k++){
			outArray[k] = (long)inArrayValue[k];
		}
		for(int k=length; k < arrayLength; k++){
			outArray[k] = 0;
		}
	}
}

void setLongMatrix
  (long** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jlongArray inRow;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jlongArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				inMatrixValue[k] = toLongArray(inRow, matrixColumns);
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jlongArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				inMatrixValue[k] = toLongArray(inRow, matrixColumns);
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				inMatrixValue[k] = (long*) malloc(matrixColumns * sizeof(long));
			}
		}
	}
}

void setLongMatrixRegion
  (long* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jlongArray inRow;
		int index = 0;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jlongArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setLongArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jlongArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setLongArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				for (int h = 0; h < matrixColumns; h++) {
					inMatrixValue[index] = 0;
					index++;
				}
			}
		}
	}
}

long toLong
  (jlong value)
{
	return (long) value;
}

long* toLongArray
  (jlongArray arrayValue, jint arrayLength)
{
	long* outArray = (long*)calloc(sizeof(long), arrayLength);
	jlong* inArrayValue = (*javaEnv)->GetLongArrayElements(javaEnv, arrayValue, NULL);
	//jlong* inArrayValue = (*javaEnv)->GetPrimitiveArrayCritical(javaEnv, arrayValue, NULL);
	int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
	int length = arrayLength < inArrayLength ? arrayLength : inArrayLength;
	for(int k=0; k < length; k++){
		outArray[k] = (long)inArrayValue[k];
	}
	for(int k=length; k < arrayLength; k++){
		outArray[k] = 0;
	}
	return outArray;
}

long** toLongMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	long** inMatrixValue = (long**) malloc(matrixRows * sizeof(long*));
    int      inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
	jlongArray inRow;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jlongArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			inMatrixValue[k] = toLongArray(inRow, matrixColumns);
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jlongArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			inMatrixValue[k] = toLongArray(inRow, matrixColumns);
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			inMatrixValue[k] = (long*) malloc(matrixColumns * sizeof(long));
		}
    }
	return inMatrixValue;
}

long* toLongMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    long* inMatrixValue = (long*) malloc(matrixRows * matrixColumns * sizeof(long*));
    int  inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
	jlongArray inRow;
	int index = 0;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jlongArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setLongArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jlongArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setLongArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			for (int h = 0; h < matrixColumns; h++) {
				inMatrixValue[index] = 0;
				index++;
			}
		}
    }
	return inMatrixValue;
}

void setJlongArray
  (jlongArray outArray, long* arrayValue, int* arrayLength)
{
	if (arrayValue != NULL && arrayValue != NULL && arrayLength != NULL) {
		jlong* jarrayValue = (jlong*)calloc(sizeof(jlong), *arrayLength);
		for(int k=0; k < *arrayLength; k++){
			jarrayValue[k] = (jlong) arrayValue[k];
		}
		int outArrayLength = (*javaEnv)->GetArrayLength(javaEnv, outArray);
		if (outArrayLength < *arrayLength) {
			(*javaEnv)->SetLongArrayRegion(javaEnv, outArray, 0, outArrayLength, jarrayValue);
		} else {
			(*javaEnv)->SetLongArrayRegion(javaEnv, outArray, 0, *arrayLength, jarrayValue);
		}
	}
}

void setJlongMatrix
  (jobjectArray matrix, long** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL) {
		jlongArray rowValue;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJlongArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJlongArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		}
	}
}

void setJlongMatrixRegion
  (jobjectArray matrix, long* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL && matrixColumns != NULL) {
		jlongArray rowValue;
		int index = 0;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJlongArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJlongArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		}
	}
}

jlong toJlong
  (long value)
{
	return (jlong) value;
}

jlongArray toJlongArray
  (long* arrayValue, int* arrayLength)
{
	if (arrayValue == NULL || arrayLength == NULL) {
		return (*javaEnv)->NewLongArray(javaEnv, 0);
	} else {
		jlong* jarrayValue = (jlong*)calloc(sizeof(jlong), *arrayLength);
		for(int k=0; k < *arrayLength; k++){
			jarrayValue[k] = (jlong) arrayValue[k];
		}
		jlongArray outArray = (*javaEnv)->NewLongArray(javaEnv, *arrayLength);
		(*javaEnv)->SetLongArrayRegion(javaEnv, outArray, 0, *arrayLength, jarrayValue);
		return outArray;
	}
}

jobjectArray toJlongMatrix
  (long** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv,"[J"), NULL);
	} else {
		jlongArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv,"[J"), NULL);
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJlongArray(matrixValue[k], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
		}
		return matrix;
	}
}

jobjectArray toJlongMatrixRegion
  (long* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL || matrixColumns == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv,"[J"), NULL);
	} else {
		jlongArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv,"[J"), NULL);
		int index = 0;
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJlongArray(&matrixValue[index], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			index += *matrixColumns;
		}
		return matrix;
	}
}


////////////////////////////////////////////////////
//                     Float                     //
////////////////////////////////////////////////////
jobject newFloatObject
  (float* value)
{
	if (value == NULL) {
		return NULL;
	} else {
		jclass cls = (*javaEnv)->FindClass(javaEnv, "Ljava/lang/Float;");
		jmethodID constructor = (*javaEnv)->GetMethodID(javaEnv, cls, "<init>", "(F)V");
		return (*javaEnv)->NewObject(javaEnv, cls, constructor, *value);
	}
}

void setFloatValue
  (jobject object, float* value)
{
    if (! (*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
		jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
		jfieldID field = (*javaEnv)->GetFieldID(javaEnv, cls, "value", "F");
		if (value == NULL) {
			(*javaEnv)->SetFloatField(javaEnv, object, field, 0.0);
		} else {
			(*javaEnv)->SetFloatField(javaEnv, object, field, *value);
		}
    }
}

float getFloatValue
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
    	return 0.0;
    } else {
		jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
		jfieldID field = (*javaEnv)->GetFieldID(javaEnv, cls, "value", "F");
		return (*javaEnv)->GetFloatField(javaEnv, object, field);
    }
}

float* floatPtr
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
    	return NULL;
    } else {
    	float* ptr = (float*) malloc(sizeof(float));
    	*ptr = getFloatValue(object);
    	return ptr;
    }
}

void setFloatPtrValue
  (float* floatPtr, float value)
{
	if (floatPtr != NULL) {
		*floatPtr = value;
	}
}

float getFloatPtrValue
  (float* floatPtr)
{
	if (floatPtr == NULL) {
		return 0;
	} else {
		return *floatPtr;
	}
}

float** toFloatMatrixPtr
  (float* matrix, int rows, int columns)
{
	float** matrixPtr = (float**) malloc(rows * sizeof(float*));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
    	matrixPtr[row] = (float*) malloc(columns * sizeof(float));
	    for (int column = 0; column < columns; column++ ) {
	    	matrixPtr[row][column] = matrix[index++];
	    }
    }
	return matrixPtr;
}

float* toFloatMatrixRegionPtr
  (float** matrix, int rows, int columns)
{
	float* matrixRegionPtr = (float*) malloc(rows * columns * sizeof(float));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
	    for (int column = 0; column < columns; column++ ) {
	    	matrixRegionPtr[index++] = matrix[row][column];
	    }
    }
	return matrixRegionPtr;
}

void setFloatArray
  (float* outArray, jfloatArray arrayValue, jint arrayLength)
{
	if (outArray != NULL && arrayLength > 0) {
		jfloat* inArrayValue = (*javaEnv)->GetFloatArrayElements(javaEnv, arrayValue, NULL);
		int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
		int length = arrayLength < inArrayLength ? arrayLength : inArrayLength;
		for(int k=0; k < length; k++){
			outArray[k] = (float)inArrayValue[k];
		}
		for(int k=length; k < arrayLength; k++){
			outArray[k] = 0.0;
		}
	}
}

void setFloatMatrix
  (float** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jfloatArray inRow;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jfloatArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				inMatrixValue[k] = toFloatArray(inRow, matrixColumns);
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jfloatArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				inMatrixValue[k] = toFloatArray(inRow, matrixColumns);
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				inMatrixValue[k] = (float*) malloc(matrixColumns * sizeof(float));
			}
		}
	}
}

void setFloatMatrixRegion
  (float* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jfloatArray inRow;
		int index = 0;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jfloatArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setFloatArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jfloatArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setFloatArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				for (int h = 0; h < matrixColumns; h++) {
					inMatrixValue[index] = 0.0;
					index++;
				}
			}
		}
	}
}

float toFloat
  (jfloat value)
{
	return (float) value;
}

float* toFloatArray
  (jfloatArray arrayValue, jint arrayLength)
{
	float* outArray = (float*)calloc(sizeof(float), arrayLength);
	jfloat* inArrayValue = (*javaEnv)->GetFloatArrayElements(javaEnv, arrayValue, NULL);
	int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
	int length = arrayLength < inArrayLength ? arrayLength : inArrayLength;
	for(int k=0; k < length; k++){
		outArray[k] = (float)inArrayValue[k];
	}
	for(int k=length; k < arrayLength; k++){
		outArray[k] = 0.0;
	}
	return outArray;
}

float** toFloatMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	float** inMatrixValue = (float**) malloc(matrixRows * sizeof(float*));
    int     inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
	jfloatArray inRow;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jfloatArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			inMatrixValue[k] = toFloatArray(inRow, matrixColumns);
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jfloatArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			inMatrixValue[k] = toFloatArray(inRow, matrixColumns);
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			inMatrixValue[k] = (float*) malloc(matrixColumns * sizeof(float));
		}
    }
	return inMatrixValue;
}

float* toFloatMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    float* inMatrixValue = (float*) malloc(matrixRows * matrixColumns * sizeof(float*));
    int  inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
	jfloatArray inRow;
	int index = 0;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jfloatArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setFloatArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jfloatArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setFloatArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			for (int h = 0; h < matrixColumns; h++) {
				inMatrixValue[index] = 0.0;
				index++;
			}
		}
    }
	return inMatrixValue;
}

void setJfloatArray
  (jfloatArray outArray, float* arrayValue, int* arrayLength)
{
	if (arrayValue != NULL && arrayValue != NULL && arrayLength != NULL) {
		jfloat* jarrayValue = (jfloat*)calloc(sizeof(jfloat), *arrayLength);
		for(int k=0; k < *arrayLength; k++){
			jarrayValue[k] = (jfloat) arrayValue[k];
		}
		int outArrayLength = (*javaEnv)->GetArrayLength(javaEnv, outArray);
		if (outArrayLength < *arrayLength) {
			(*javaEnv)->SetFloatArrayRegion(javaEnv, outArray, 0, outArrayLength, jarrayValue);
		} else {
			(*javaEnv)->SetFloatArrayRegion(javaEnv, outArray, 0, *arrayLength, jarrayValue);
		}
	}
}

void setJfloatMatrix
  (jobjectArray matrix, float** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL) {
		jfloatArray rowValue;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJfloatArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJfloatArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		}
	}
}

void setJfloatMatrixRegion
  (jobjectArray matrix, float* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL && matrixColumns != NULL) {
		jfloatArray rowValue;
		int index = 0;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJfloatArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJfloatArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		}
	}
}

jfloat toJfloat
  (float value)
{
	return (jfloat) value;
}

jfloatArray toJfloatArray
  (float* arrayValue, int* arrayLength)
{
	if (arrayValue == NULL || arrayLength == NULL) {
		return (*javaEnv)->NewFloatArray(javaEnv, 0);
	} else {
		jfloat* jarrayValue = (jfloat*)calloc(sizeof(jfloat), *arrayLength);
		for(int k=0; k < *arrayLength; k++){
			jarrayValue[k] = (jfloat) arrayValue[k];
		}
		jfloatArray outArray = (*javaEnv)->NewFloatArray(javaEnv, *arrayLength);
		(*javaEnv)->SetFloatArrayRegion(javaEnv, outArray, 0, *arrayLength, jarrayValue);
		return outArray;
	}
}

jobjectArray toJfloatMatrix
  (float** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv,"[F"), NULL);
	} else {
		jfloatArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv,"[F"), NULL);
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJfloatArray(matrixValue[k], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
		}
		return matrix;
	}
}

jobjectArray toJfloatMatrixRegion
  (float* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL || matrixColumns == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv,"[F"), NULL);
	} else {
		jfloatArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv,"[F"), NULL);
		int index = 0;
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJfloatArray(&matrixValue[index], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			index += *matrixColumns;
		}
		return matrix;
	}
}


////////////////////////////////////////////////////
//                     Double                     //
////////////////////////////////////////////////////
jobject newDoubleObject
  (double* value)
{
	if (value == NULL) {
		return NULL;
	} else {
		jclass cls = (*javaEnv)->FindClass(javaEnv, "Ljava/lang/Double;");
		jmethodID constructor = (*javaEnv)->GetMethodID(javaEnv, cls, "<init>", "(D)V");
		return (*javaEnv)->NewObject(javaEnv, cls, constructor, *value);
	}
}

void setDoubleValue
  (jobject object, double* value)
{
    if (! (*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
		jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
		jfieldID field = (*javaEnv)->GetFieldID(javaEnv, cls, "value", "D");
		if (value == NULL) {
			(*javaEnv)->SetDoubleField(javaEnv, object, field, 0.0);
		} else {
			(*javaEnv)->SetDoubleField(javaEnv, object, field, *value);
		}
    }
}

double getDoubleValue
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
    	return 0.0;
    } else {
		jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
		jfieldID field = (*javaEnv)->GetFieldID(javaEnv, cls, "value", "D");
		return (*javaEnv)->GetDoubleField(javaEnv, object, field);
    }
}

double* doublePtr
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
    	return NULL;
    } else {
    	double* ptr = (double*) malloc(sizeof(double));
    	*ptr = getDoubleValue(object);
    	return ptr;
    }
}

void setDoublePtrValue
  (double* doublePtr, double value)
{
	if (doublePtr != NULL) {
		*doublePtr = value;
	}
}

double getDoublePtrValue
  (double* doublePtr)
{
	if (doublePtr == NULL) {
		return 0;
	} else {
		return *doublePtr;
	}
}

double** toDoubleMatrixPtr
  (double* matrix, int rows, int columns)
{
	double** matrixPtr = (double**) malloc(rows * sizeof(double*));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
    	matrixPtr[row] = (double*) malloc(columns * sizeof(double));
	    for (int column = 0; column < columns; column++ ) {
	    	matrixPtr[row][column] = matrix[index++];
	    }
    }
	return matrixPtr;
}

double* toDoubleMatrixRegionPtr
  (double** matrix, int rows, int columns)
{
	double* matrixRegionPtr = (double*) malloc(rows * columns * sizeof(double));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
	    for (int column = 0; column < columns; column++ ) {
	    	matrixRegionPtr[index++] = matrix[row][column];
	    }
    }
	return matrixRegionPtr;
}

void setDoubleArray
  (double* outArray, jdoubleArray arrayValue, jint arrayLength)
{
	if (outArray != NULL && arrayLength > 0) {
		jdouble* inArrayValue = (*javaEnv)->GetDoubleArrayElements(javaEnv, arrayValue, NULL);
		int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
		int length = arrayLength < inArrayLength ? arrayLength : inArrayLength;
		for(int k=0; k < length; k++){
			outArray[k] = (double)inArrayValue[k];
		}
		for(int k=length; k < arrayLength; k++){
			outArray[k] = 0.0;
		}
	}
}

void setDoubleMatrix
  (double** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jdoubleArray inRow;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jdoubleArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				inMatrixValue[k] = toDoubleArray(inRow, matrixColumns);
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jdoubleArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				inMatrixValue[k] = toDoubleArray(inRow, matrixColumns);
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				inMatrixValue[k] = (double*) malloc(matrixColumns * sizeof(double));
			}
		}
	}
}

void setDoubleMatrixRegion
  (double* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jdoubleArray inRow;
		int index = 0;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jdoubleArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setDoubleArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jdoubleArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setDoubleArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				for (int h = 0; h < matrixColumns; h++) {
					inMatrixValue[index] = 0.0;
					index++;
				}
			}
		}
	}
}

double toDouble
  (jdouble value)
{
	return (double) value;
}

double* toDoubleArray
  (jdoubleArray arrayValue, jint arrayLength)
{
	double* outArray = (double*)calloc(sizeof(double), arrayLength);
	jdouble* inArrayValue = (*javaEnv)->GetDoubleArrayElements(javaEnv, arrayValue, NULL);
	int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
	int length = arrayLength < inArrayLength ? arrayLength : inArrayLength;
	for(int k=0; k < length; k++){
		outArray[k] = (double)inArrayValue[k];
	}
	for(int k=length; k < arrayLength; k++){
		outArray[k] = 0.0;
	}
	return outArray;
}

double** toDoubleMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	double** inMatrixValue = (double**) malloc(matrixRows * sizeof(double*));
    int      inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
	jdoubleArray inRow;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jdoubleArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			inMatrixValue[k] = toDoubleArray(inRow, matrixColumns);
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jdoubleArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			inMatrixValue[k] = toDoubleArray(inRow, matrixColumns);
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			inMatrixValue[k] = (double*) malloc(matrixColumns * sizeof(double));
		}
    }
	return inMatrixValue;
}

double* toDoubleMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    double* inMatrixValue = (double*) malloc(matrixRows * matrixColumns * sizeof(double*));
    int  inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
	jdoubleArray inRow;
	int index = 0;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jdoubleArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setDoubleArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jdoubleArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setDoubleArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			for (int h = 0; h < matrixColumns; h++) {
				inMatrixValue[index] = 0.0;
				index++;
			}
		}
    }
	return inMatrixValue;
}

void setJdoubleArray
  (jdoubleArray outArray, double* arrayValue, int* arrayLength)
{
	if (arrayValue != NULL && arrayValue != NULL && arrayLength != NULL) {
		jdouble* jarrayValue = (jdouble*)calloc(sizeof(jdouble), *arrayLength);
		for(int k=0; k < *arrayLength; k++){
			jarrayValue[k] = (jdouble) arrayValue[k];
		}
		int outArrayLength = (*javaEnv)->GetArrayLength(javaEnv, outArray);
		if (outArrayLength < *arrayLength) {
			(*javaEnv)->SetDoubleArrayRegion(javaEnv, outArray, 0, outArrayLength, jarrayValue);
		} else {
			(*javaEnv)->SetDoubleArrayRegion(javaEnv, outArray, 0, *arrayLength, jarrayValue);
		}
	}
}

void setJdoubleMatrix
  (jobjectArray matrix, double** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL) {
		jdoubleArray rowValue;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJdoubleArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJdoubleArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		}
	}
}

void setJdoubleMatrixRegion
  (jobjectArray matrix, double* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL && matrixColumns != NULL) {
		jdoubleArray rowValue;
		int index = 0;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJdoubleArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJdoubleArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		}
	}
}

jdouble toJdouble
  (double value)
{
	return (jdouble) value;
}

jdoubleArray toJdoubleArray
  (double* arrayValue, int* arrayLength)
{
	if (arrayValue == NULL || arrayLength == NULL) {
		return (*javaEnv)->NewDoubleArray(javaEnv, 0);
	} else {
		jdouble* jarrayValue = (jdouble*)calloc(sizeof(jdouble), *arrayLength);
		for(int k=0; k < *arrayLength; k++){
			jarrayValue[k] = (jdouble) arrayValue[k];
		}
		jdoubleArray outArray = (*javaEnv)->NewDoubleArray(javaEnv, *arrayLength);
		(*javaEnv)->SetDoubleArrayRegion(javaEnv, outArray, 0, *arrayLength, jarrayValue);
		return outArray;
	}
}

jobjectArray toJdoubleMatrix
  (double** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv,"[D"), NULL);
	} else {
		jdoubleArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv,"[D"), NULL);
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJdoubleArray(matrixValue[k], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
		}
		return matrix;
	}
}

jobjectArray toJdoubleMatrixRegion
  (double* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL || matrixColumns == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv,"[D"), NULL);
	} else {
		jdoubleArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv,"[D"), NULL);
		int index = 0;
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJdoubleArray(&matrixValue[index], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			index += *matrixColumns;
		}
		return matrix;
	}
}


////////////////////////////////////////////////////
//                     Boolean                    //
////////////////////////////////////////////////////
jobject newBooleanObject
  (bool* value)
{
	if (value == NULL) {
		return NULL;
	} else {
		jclass cls = (*javaEnv)->FindClass(javaEnv, "Ljava/lang/Boolean;");
		jmethodID constructor = (*javaEnv)->GetMethodID(javaEnv, cls, "<init>", "(Z)V");
		return (*javaEnv)->NewObject(javaEnv, cls, constructor, *value);
	}
}

void setBooleanValue
  (jobject object, bool* value)
{
    if (! (*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
		jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
		jfieldID field = (*javaEnv)->GetFieldID(javaEnv, cls, "value", "Z");
		if (value == NULL) {
			(*javaEnv)->SetBooleanField(javaEnv, object, field, false);
		} else {
			(*javaEnv)->SetBooleanField(javaEnv, object, field, *value);
		}
    }
}

bool getBooleanValue
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
    	return false;
    } else {
		jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
		jfieldID field = (*javaEnv)->GetFieldID(javaEnv, cls, "value", "Z");
		return (*javaEnv)->GetBooleanField(javaEnv, object, field);
    }
}

bool* boolPtr
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
  	    return NULL;
    } else {
    	bool* ptr = (bool*) malloc(sizeof(bool));
  	    *ptr = getBooleanValue(object);
  	    return ptr;
    }
}

void setBoolPtrValue
  (bool* boolPtr, bool value)
{
	if (boolPtr != NULL) {
		*boolPtr = value;
	}
}

bool getboolPtrValue
  (bool* boolPtr)
{
	if (boolPtr == NULL) {
		return 0;
	} else {
		return *boolPtr;
	}
}

bool** toBoolMatrixPtr
  (bool* matrix, int rows, int columns)
{
	bool** matrixPtr = (bool**) malloc(rows * sizeof(bool*));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
    	matrixPtr[row] = (bool*) malloc(columns * sizeof(bool));
	    for (int column = 0; column < columns; column++ ) {
	    	matrixPtr[row][column] = matrix[index++];
	    }
    }
	return matrixPtr;
}

bool* toBoolMatrixRegionPtr
  (bool** matrix, int rows, int columns)
{
	bool* matrixRegionPtr = (bool*) malloc(rows * columns * sizeof(bool));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
	    for (int column = 0; column < columns; column++ ) {
	    	matrixRegionPtr[index++] = matrix[row][column];
	    }
    }
	return matrixRegionPtr;
}

void setBoolArray
  (bool* outArray, jbooleanArray arrayValue, jint arrayLength)
{
	if (outArray != NULL && arrayLength > 0) {
		jboolean* inArrayValue = (*javaEnv)->GetBooleanArrayElements(javaEnv, arrayValue, NULL);
		int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
		int length = arrayLength < inArrayLength ? arrayLength : inArrayLength;
		for(int k=0; k < length; k++){
			outArray[k] = (bool)inArrayValue[k];
		}
		for(int k=length; k < arrayLength; k++){
			outArray[k] = false;
		}
	}
}

void setBoolMatrix
  (bool** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jbooleanArray inRow;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jbooleanArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				inMatrixValue[k] = toBoolArray(inRow, matrixColumns);
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jbooleanArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				inMatrixValue[k] = toBoolArray(inRow, matrixColumns);
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				inMatrixValue[k] = (bool*) malloc(matrixColumns * sizeof(bool));
			}
		}
	}
}

void setBoolMatrixRegion
  (bool* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jbooleanArray inRow;
		int index = 0;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jbooleanArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setBoolArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jbooleanArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setBoolArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				for (int h = 0; h < matrixColumns; h++) {
					inMatrixValue[index] = false;
					index++;
				}
			}
		}
	}
}

bool toBool
  (jboolean value)
{
	return (bool) value;
}

bool* toBoolArray
  (jbooleanArray arrayValue, jint arrayLength)
{
	bool* outArray = (bool*)calloc(sizeof(bool), arrayLength);
	jboolean* inArrayValue = (*javaEnv)->GetBooleanArrayElements(javaEnv, arrayValue, NULL);
	int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
	int length = arrayLength < inArrayLength ? arrayLength : inArrayLength;
	for(int k=0; k < length; k++){
		outArray[k] = (bool)inArrayValue[k];
	}
	for(int k=length; k < arrayLength; k++){
		outArray[k] = false;
	}
	return outArray;
}

bool** toBoolMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	bool** inMatrixValue = (bool**) malloc(matrixRows * sizeof(bool*));
    int    inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
	jbooleanArray inRow;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jbooleanArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			inMatrixValue[k] = toBoolArray(inRow, matrixColumns);
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jbooleanArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			inMatrixValue[k] = toBoolArray(inRow, matrixColumns);
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			inMatrixValue[k] = (bool*) malloc(matrixColumns * sizeof(bool));
		}
    }
	return inMatrixValue;
}

bool* toBoolMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    bool* inMatrixValue = (bool*) malloc(matrixRows * matrixColumns * sizeof(bool*));
    int  inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
	jbooleanArray inRow;
	int index = 0;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jbooleanArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setBoolArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jbooleanArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setBoolArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			for (int h = 0; h < matrixColumns; h++) {
				inMatrixValue[index] = false;
				index++;
			}
		}
    }
	return inMatrixValue;
}

void setJboolArray
  (jbooleanArray outArray, bool* arrayValue, int* arrayLength)
{
	if (arrayValue != NULL && arrayValue != NULL && arrayLength != NULL) {
		jboolean* jarrayValue = (jboolean*)calloc(sizeof(jboolean), *arrayLength);
		for(int k=0; k < *arrayLength; k++){
			jarrayValue[k] = (jboolean) arrayValue[k];
		}
		int outArrayLength = (*javaEnv)->GetArrayLength(javaEnv, outArray);
		if (outArrayLength < *arrayLength) {
			(*javaEnv)->SetBooleanArrayRegion(javaEnv, outArray, 0, outArrayLength, jarrayValue);
		} else {
			(*javaEnv)->SetBooleanArrayRegion(javaEnv, outArray, 0, *arrayLength, jarrayValue);
		}
	}
}

void setJboolMatrix
  (jobjectArray matrix, bool** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL) {
		jbooleanArray rowValue;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJbooleanArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJbooleanArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		}
	}
}

void setJboolMatrixRegion
  (jobjectArray matrix, bool* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL && matrixColumns != NULL) {
		jbooleanArray rowValue;
		int index = 0;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJbooleanArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJbooleanArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		}
	}
}

jboolean toJboolean
  (bool value)
{
	return (jboolean) value;
}

jbooleanArray toJbooleanArray
  (bool* arrayValue, int* arrayLength)
{
	if (arrayValue == NULL || arrayLength == NULL) {
		return (*javaEnv)->NewBooleanArray(javaEnv, 0);
	} else {
		jboolean* jarrayValue = (jboolean*)calloc(sizeof(jboolean), *arrayLength);
		for(int k=0; k < *arrayLength; k++){
			jarrayValue[k] = (jboolean) arrayValue[k];
		}
		jbooleanArray outArray = (*javaEnv)->NewBooleanArray(javaEnv, *arrayLength);
		(*javaEnv)->SetBooleanArrayRegion(javaEnv, outArray, 0, *arrayLength, jarrayValue);
		return outArray;
	}
}

jobjectArray toJbooleanMatrix
  (bool** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv,"[Z"), NULL);
	} else {
		jbooleanArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv,"[Z"), NULL);
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJbooleanArray(matrixValue[k], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
		}
		return matrix;
	}
}

jobjectArray toJbooleanMatrixRegion
  (bool* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL || matrixColumns == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv,"[Z"), NULL);
	} else {
		jbooleanArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv,"[Z"), NULL);
		int index = 0;
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJbooleanArray(&matrixValue[index], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			index += *matrixColumns;
		}
		return matrix;
	}
}


////////////////////////////////////////////////////
//                    Character                   //
////////////////////////////////////////////////////
jobject newCharacterObject
  (char* value)
{
	if (value == NULL) {
		return NULL;
	} else {
		jclass cls = (*javaEnv)->FindClass(javaEnv, "Ljava/lang/Character;");
		jmethodID constructor = (*javaEnv)->GetMethodID(javaEnv, cls, "<init>", "(C)V");
		return (*javaEnv)->NewObject(javaEnv, cls, constructor, *value);
	}
}

void setCharacterValue
  (jobject object, char* value)
{
    if (! (*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
		jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
		jfieldID field = (*javaEnv)->GetFieldID(javaEnv, cls, "value", "C");
		if (value == NULL) {
			(*javaEnv)->SetCharField(javaEnv, object, field, '\0');
		} else {
			(*javaEnv)->SetCharField(javaEnv, object, field, *value);
		}
    }
}

char getCharacterValue
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
    	return '\0';
    } else {
		jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
		jfieldID field = (*javaEnv)->GetFieldID(javaEnv, cls, "value", "C");
		return (*javaEnv)->GetCharField(javaEnv, object, field);
    }
}

char* charPtr
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
	    return NULL;
    } else {
    	char* ptr = (char*) malloc(sizeof(char));
	    *ptr = getCharacterValue(object);
	    return ptr;
    }
}

void setCharPtrValue
  (char* charPtr, char value)
{
	if (charPtr != NULL) {
		*charPtr = value;
	}
}

char** toCharMatrixPtr
  (char* matrix, int rows, int columns)
{
	char** matrixPtr = (char**) malloc(rows * sizeof(char*));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
    	matrixPtr[row] = (char*) malloc(columns * sizeof(char));
	    for (int column = 0; column < columns; column++ ) {
	    	matrixPtr[row][column] = matrix[index++];
	    }
    }
	return matrixPtr;
}

char* toCharMatrixRegionPtr
  (char** matrix, int rows, int columns)
{
	char* matrixRegionPtr = (char*) malloc(rows * columns * sizeof(char));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
	    for (int column = 0; column < columns; column++ ) {
	    	matrixRegionPtr[index++] = matrix[row][column];
	    }
    }
	return matrixRegionPtr;
}

char getCharPtrValue
  (char* charPtr)
{
	if (charPtr == NULL) {
		return 0;
	} else {
		return *charPtr;
	}
}

void setCharArray
  (char* outArray, jcharArray arrayValue, jint arrayLength)
{
	if (outArray != NULL && arrayLength > 0) {
		jchar* inArrayValue = (*javaEnv)->GetCharArrayElements(javaEnv, arrayValue, NULL);
		int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
		int length = arrayLength < inArrayLength ? arrayLength : inArrayLength;
		for(int k=0; k < length; k++){
			outArray[k] = (char)inArrayValue[k];
		}
		for(int k=length; k < arrayLength; k++){
			outArray[k] = '\0';
		}
	}
}

void setCharMatrix
  (char** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jcharArray inRow;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jcharArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				inMatrixValue[k] = toCharArray(inRow, matrixColumns);
			}

		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jcharArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				inMatrixValue[k] = toCharArray(inRow, matrixColumns);
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				inMatrixValue[k] = (char*) malloc(matrixColumns * sizeof(char));
			}
		}
	}
}

void setCharMatrixRegion
  (char* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jcharArray inRow;
		int index = 0;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jcharArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setCharArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jcharArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setCharArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				for (int h = 0; h < matrixColumns; h++) {
					inMatrixValue[index] = 0;
					index++;
				}
			}
		}
	}
}

char toChar
  (jchar value)
{
	return (char) value;
}

char* toCharArray
  (jcharArray arrayValue, jint arrayLength)
{
	char* outArray = (char*)calloc(sizeof(char), arrayLength);
	jchar* inArrayValue = (*javaEnv)->GetCharArrayElements(javaEnv, arrayValue, NULL);
	int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
	int length = arrayLength < inArrayLength ? arrayLength : inArrayLength;
	for(int k=0; k < length; k++){
		outArray[k] = (char)inArrayValue[k];
	}
	for(int k=length; k < arrayLength; k++){
		outArray[k] = '\0';
	}
	return outArray;
}

char** toCharMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	char** inMatrixValue = (char**) malloc(matrixRows * sizeof(char*));
    int    inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
	jcharArray inRow;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jcharArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			inMatrixValue[k] = toCharArray(inRow, matrixColumns);
		}

    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jcharArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			inMatrixValue[k] = toCharArray(inRow, matrixColumns);
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			inMatrixValue[k] = (char*) malloc(matrixColumns * sizeof(char));
		}
    }
	return inMatrixValue;
}

char* toCharMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    char* inMatrixValue = (char*) malloc(matrixRows * matrixColumns * sizeof(char*));
    int  inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
	jcharArray inRow;
	int index = 0;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jcharArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setCharArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jcharArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setCharArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			for (int h = 0; h < matrixColumns; h++) {
				inMatrixValue[index] = 0;
				index++;
			}
		}
    }
	return inMatrixValue;
}

void setJcharArray
  (jcharArray outArray, char* arrayValue, int* arrayLength)
{
	if (arrayValue != NULL && arrayValue != NULL && arrayLength != NULL) {
		jchar* jarrayValue = (jchar*)calloc(sizeof(jchar), *arrayLength);
		for(int k=0; k < *arrayLength; k++){
			jarrayValue[k] = (jchar) arrayValue[k];
		}
		int outArrayLength = (*javaEnv)->GetArrayLength(javaEnv, outArray);
		if (outArrayLength < *arrayLength) {
			(*javaEnv)->SetCharArrayRegion(javaEnv, outArray, 0, outArrayLength, jarrayValue);
		} else {
			(*javaEnv)->SetCharArrayRegion(javaEnv, outArray, 0, *arrayLength, jarrayValue);
		}
	}
}

void setJcharMatrix
  (jobjectArray matrix, char** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL) {
		jcharArray rowValue;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJcharArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJcharArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		}
	}
}

jchar toJchar
  (char value)
{
	return (jchar) value;
}

jcharArray toJcharArray
  (char* arrayValue, int* arrayLength)
{
	if (arrayValue == NULL || arrayLength == NULL) {
		return (*javaEnv)->NewCharArray(javaEnv, 0);

	} else {
		jchar* jarrayValue = (jchar*)calloc(sizeof(jchar), *arrayLength);
		for(int k=0; k < *arrayLength; k++){
			jarrayValue[k] = (jchar) arrayValue[k];
		}
		jcharArray outArray = (*javaEnv)->NewCharArray(javaEnv, *arrayLength);
		(*javaEnv)->SetCharArrayRegion(javaEnv, outArray, 0, *arrayLength, jarrayValue);
		return outArray;
	}
}

jobjectArray toJcharMatrix
  (char** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv,"[C"), NULL);
	} else {
		jcharArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv,"[C"), NULL);
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJcharArray(matrixValue[k], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
		}
		return matrix;
	}
}

void setJcharMatrixRegion
  (jobjectArray matrix, char* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL && matrixColumns != NULL) {
		jcharArray rowValue;
		int index = 0;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJcharArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJcharArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		}
	}
}

jobjectArray toJcharMatrixRegion
  (char* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL || matrixColumns == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv,"[C"), NULL);
	} else {
		jcharArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv,"[C"), NULL);
		int index = 0;
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJcharArray(&matrixValue[index], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			index += *matrixColumns;
		}
		return matrix;
	}
}


////////////////////////////////////////////////////
//                     String                     //
////////////////////////////////////////////////////
jobject newStringObject
  (const char** value)
{
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Ljava/lang/StringBuilder;");
	if (value == NULL) {
		return NULL;
	} else {
		jmethodID constructor = (*javaEnv)->GetMethodID(javaEnv, cls, "<init>", "(Ljava/lang/String;)V");
		return (*javaEnv)->NewObject(javaEnv, cls, constructor, toJstring(*value));
	}
}

void setStringValue
  (jobject object, const char** value)
{
    if (! (*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
		jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
        jmethodID method = (*javaEnv)->GetMethodID(javaEnv, cls, "length", "()I");
        jint j_outvalue = (*javaEnv)->CallIntMethod(javaEnv, object, method);
        if (! (*javaEnv)->ExceptionOccurred(javaEnv)) {
        	if (j_outvalue == 0) {
				method = (*javaEnv)->GetMethodID(javaEnv, cls, "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;");
        		if (value == NULL) {
					(*javaEnv)->CallObjectMethod(javaEnv, object, method, toJstring(""));
        		} else {
					(*javaEnv)->CallObjectMethod(javaEnv, object, method, toJstring(*value));
        		}
			} else {
				method = (*javaEnv)->GetMethodID(javaEnv, cls, "replace", "(IILjava/lang/String;)Ljava/lang/StringBuilder;");
        		if (value == NULL) {
					(*javaEnv)->CallObjectMethod(javaEnv, object, method, 0, j_outvalue, toJstring(""));
        		} else {
					(*javaEnv)->CallObjectMethod(javaEnv, object, method, 0, j_outvalue, toJstring(*value));
        		}
        	}
        }
    }
}

const char* getStringValue
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
    	return 0;
    } else {
		jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
        jmethodID method = (*javaEnv)->GetMethodID(javaEnv, cls, "toString", "()Ljava/lang/String;");
        jstring j_outvalue = (*javaEnv)->CallObjectMethod(javaEnv, object, method);
        if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
        	return 0;
        } else {
            return toString(j_outvalue);
        }
    }
}

const char** StringPtr
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
	    return NULL;
    } else {
    	const char** ptr = (const char**) malloc(sizeof(const char*));
	    *ptr = getStringValue(object);
	    return ptr;
    }
}

void setStringPtrValue
  (const char** charPtr, const char* value)
{
	if (charPtr != NULL) {
		*charPtr = value;
	}
}

const char* getStringPtrValue
  (const char** charPtr)
{
	if (charPtr == NULL) {
		return 0;
	} else {
		return *charPtr;
	}
}

const char*** toStringMatrixPtr
  (const char** matrix, int rows, int columns)
{
	const char*** matrixPtr = (const char***) malloc(rows * sizeof(const char**));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
    	matrixPtr[row] = (const char**) malloc(columns * sizeof(const char*));
	    for (int column = 0; column < columns; column++ ) {
	    	matrixPtr[row][column] = matrix[index++];
	    }
    }
	return matrixPtr;
}

const char** toStringMatrixRegionPtr
  (const char*** matrix, int rows, int columns)
{
	const char** matrixRegionPtr = (const char**) malloc(rows * columns * sizeof(const char*));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
	    for (int column = 0; column < columns; column++ ) {
	    	matrixRegionPtr[index++] = matrix[row][column];
	    }
    }
	return matrixRegionPtr;
}

void setStringArray
  (const char** inArrayValue, jobjectArray arrayValue, jint arrayLength)
{
	int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
	jstring inString;
	if (arrayLength < inArrayLength) {
		for (int k = 0; k < arrayLength; k++) {
			inString = (*javaEnv)->GetObjectArrayElement(javaEnv, arrayValue, k);
			inArrayValue[k] = toString(inString);
		}
	} else {
		for (int k = 0; k < inArrayLength; k++) {
			inString = (*javaEnv)->GetObjectArrayElement(javaEnv, arrayValue, k);
			inArrayValue[k] = toString(inString);
		}
	}
}

void setStringMatrix
  (const char*** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    int inMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
    jobjectArray inRow;
    if (matrixRows < inMatrixRows) {
    	for(int k = 0; k < matrixRows; k++) {
    		inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
    		inMatrixValue[k] = toStringArray(inRow, matrixColumns);
    	}
    } else {
    	for(int k = 0; k < inMatrixRows; k++) {
    		inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
    		inMatrixValue[k] = toStringArray(inRow, matrixColumns);
    	}
    }
}

void setStringMatrixRegion
  (const char** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jobjectArray inRow;
		int index = 0;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setStringArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setStringArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				for (int h = 0; h < matrixColumns; h++) {
					inMatrixValue[index] = NULL;
					index++;
				}
			}
		}
	}
}

const char* toString
  (jstring value)
{
	if (value == NULL) {
		return NULL;
	} else {
		return (*javaEnv)->GetStringUTFChars(javaEnv, (jstring)value, NULL);
	}
}

const char** toStringArray
  (jobjectArray arrayValue, jint arrayLength)
{
	const char** inArrayValue = (const char**) malloc(arrayLength * sizeof(const char*));
	int          inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
	jstring inString;

	if (arrayLength < inArrayLength) {
		for (int k = 0; k < arrayLength; k++) {
			inString = (*javaEnv)->GetObjectArrayElement(javaEnv, arrayValue, k);
			inArrayValue[k] = toString(inString);
		}
	} else {
		for (int k = 0; k < inArrayLength; k++) {
			inString = (*javaEnv)->GetObjectArrayElement(javaEnv, arrayValue, k);
			inArrayValue[k] = toString(inString);
		}
		for (int k = inArrayLength; k < arrayLength; k++) {
			inArrayValue[k] = NULL;
		}
	}
	return inArrayValue;
}

const char*** toStringMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    const char*** inMatrixValue = (const char***) malloc(matrixRows * sizeof(const char**));
    int           inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
    jobjectArray inRow;
    if (matrixRows < inMatrixRows) {
    	for(int k = 0; k < matrixRows; k++) {
    		inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
    		inMatrixValue[k] = toStringArray(inRow, matrixColumns);
    	}
    } else {
    	for(int k = 0; k < inMatrixRows; k++) {
    		inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
    		inMatrixValue[k] = toStringArray(inRow, matrixColumns);
    	}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			inMatrixValue[k] = (const char**) malloc(matrixColumns * sizeof(const char*));
			for (int h = 0; h < matrixColumns; h++) {
				inMatrixValue[k][h] = NULL;
			}
		}
    }
	return inMatrixValue;
}

const char** toStringMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    const char** inMatrixValue = (const char**) malloc(matrixRows * matrixColumns * sizeof(const char**));
    int  inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
    jobjectArray inRow;
	int index = 0;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setStringArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setStringArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			for (int h = 0; h < matrixColumns; h++) {
				inMatrixValue[index] = NULL;
				index++;
			}
		}
    }
	return inMatrixValue;
}

void setJstringArray
  (jobjectArray outArray, const char** arrayValue, int* arrayLength)
{
	if (outArray != NULL && arrayValue != NULL && arrayLength != NULL) {
		jstring string;
		int outArrayLength = (*javaEnv)->GetArrayLength(javaEnv, outArray);
		if (outArrayLength < *arrayLength) {
			for (int k=0; k < outArrayLength; k++) {
			    string = toJstring(arrayValue[k]);
			    (*javaEnv)->SetObjectArrayElement(javaEnv, outArray, k, string);
			}
		} else {
			for (int k=0; k < *arrayLength; k++) {
				string = toJstring(arrayValue[k]);
				(*javaEnv)->SetObjectArrayElement(javaEnv, outArray, k, string);
			}
		}
	}
}

void setJstringMatrix
  (jobjectArray matrix, const char*** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL) {
		jobjectArray rowValue;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJstringArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJstringArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		}
	}
}

void setJstringMatrixRegion
  (jobjectArray matrix, const char** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL && matrixColumns != NULL) {
		jobjectArray rowValue;
		int index = 0;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJstringArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJstringArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		}
	}
}

jstring toJstring
  (const char* string)
{
	if (string == NULL) {
		return NULL;
	} else {
		return (*javaEnv)->NewStringUTF(javaEnv, string);
	}
}

jobjectArray toJstringArray
  (const char** arrayValue, int* arrayLength)
{
	if (arrayValue == NULL || arrayLength == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv, "Ljava/lang/String;"), NULL);
	} else {
		jobjectArray outArray = (*javaEnv)->NewObjectArray(javaEnv, *arrayLength, (*javaEnv)->FindClass(javaEnv, "Ljava/lang/String;"), NULL);
		jstring string;
		for (int k=0; k < *arrayLength; k++) {
			string = toJstring(arrayValue[k]);
			(*javaEnv)->SetObjectArrayElement(javaEnv, outArray, k, string);
		}
		return outArray;
	}
}

jobjectArray toJstringMatrix
  (const char*** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv,"[Ljava/lang/String;"), NULL);
	} else {
		jobjectArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv,"[Ljava/lang/String;"), NULL);
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJstringArray(matrixValue[k], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
		}
		return matrix;
	}
}

jobjectArray toJstringMatrixRegion
  (const char** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL || matrixColumns == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv,"[Ljava/lang/String;"), NULL);
	} else {
		jobjectArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv,"[Ljava/lang/String;"), NULL);
		int index = 0;
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJstringArray(&matrixValue[index], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			index += *matrixColumns;
		}
		return matrix;
	}
}


//////////////////////////////////////////////////////
//                Global variables                  //
//////////////////////////////////////////////////////

void setGlobalInt(jobject object, const char* name, int number) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), newIntegerObject(&number));
}

int getGlobalInt(jobject object, const char* name) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobject j_object =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	return getIntegerValue(j_object);
}

void setGlobalIntArray(jobject object, const char* name, int* number, int length) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), toJintArray(number, &length));
}

int* getGlobalIntArray(jobject object, const char* name, int* length) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobjectArray j_objectArray =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	*length = (*javaEnv)->GetArrayLength(javaEnv, j_objectArray);
	return toIntArray(j_objectArray, *length);
}

void setGlobalIntMatrix(jobject object, const char* name, int** number, int rows, int columns) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), toJintMatrix(number, &rows, &columns));
}

int** getGlobalIntMatrix(jobject object, const char* name, int* rows, int* columns) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobjectArray j_objectArray =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	*rows = (*javaEnv)->GetArrayLength(javaEnv, j_objectArray);
	*columns = (*javaEnv)->GetArrayLength(javaEnv, (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, j_objectArray, 0));
	return toIntMatrix(j_objectArray, *rows, *columns);
}

void setGlobalByte(jobject object, const char* name, byte number) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), newByteObject(&number));
}

byte getGlobalByte(jobject object, const char* name) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobject j_object =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	return getByteValue(j_object);
}

void setGlobalByteArray(jobject object, const char* name, byte* number, int length) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), toJbyteArray(number, &length));
}

byte* getGlobalByteArray(jobject object, const char* name, int* length) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobjectArray j_objectArray =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	*length = (*javaEnv)->GetArrayLength(javaEnv, j_objectArray);
	return toByteArray(j_objectArray, *length);
}

void setGlobalByteMatrix(jobject object, const char* name, byte** number, int rows, int columns) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), toJbyteMatrix(number, &rows, &columns));
}

byte** getGlobalByteMatrix(jobject object, const char* name, int* rows, int* columns) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobjectArray j_objectArray =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	*rows = (*javaEnv)->GetArrayLength(javaEnv, j_objectArray);
	*columns = (*javaEnv)->GetArrayLength(javaEnv, (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, j_objectArray, 0));
	return toByteMatrix(j_objectArray, *rows, *columns);
}

void setGlobalShort(jobject object, const char* name, short number) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), newShortObject(&number));
}

short getGlobalShort(jobject object, const char* name) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobject j_object =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	return getShortValue(j_object);
}

void setGlobalShortArray(jobject object, const char* name, short* number, int length) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), toJshortArray(number, &length));
}

short* getGlobalShortArray(jobject object, const char* name, int* length) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobjectArray j_objectArray =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	*length = (*javaEnv)->GetArrayLength(javaEnv, j_objectArray);
	return toShortArray(j_objectArray, *length);
}

void setGlobalShortMatrix(jobject object, const char* name, short** number, int rows, int columns) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), toJshortMatrix(number, &rows, &columns));
}

short** getGlobalShortMatrix(jobject object, const char* name, int* rows, int* columns) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobjectArray j_objectArray =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	*rows = (*javaEnv)->GetArrayLength(javaEnv, j_objectArray);
	*columns = (*javaEnv)->GetArrayLength(javaEnv, (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, j_objectArray, 0));
	return toShortMatrix(j_objectArray, *rows, *columns);
}

void setGlobalLong(jobject object, const char* name, long number) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), newLongObject(&number));
}

long getGlobalLong(jobject object, const char* name) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobject j_object =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	return getLongValue(j_object);
}

void setGlobalLongArray(jobject object, const char* name, long* number, int length) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), toJlongArray(number, &length));
}

long* getGlobalLongArray(jobject object, const char* name, int* length) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobjectArray j_objectArray =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	*length = (*javaEnv)->GetArrayLength(javaEnv, j_objectArray);
	return toLongArray(j_objectArray, *length);
}

void setGlobalLongMatrix(jobject object, const char* name, long** number, int rows, int columns) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), toJlongMatrix(number, &rows, &columns));
}

long** getGlobalLongMatrix(jobject object, const char* name, int* rows, int* columns) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobjectArray j_objectArray =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	*rows = (*javaEnv)->GetArrayLength(javaEnv, j_objectArray);
	*columns = (*javaEnv)->GetArrayLength(javaEnv, (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, j_objectArray, 0));
	return toLongMatrix(j_objectArray, *rows, *columns);
}

void setGlobalDouble(jobject object, const char* name, double number) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), newDoubleObject(&number));
}

double getGlobalDouble(jobject object, const char* name) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobject j_object =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	return getDoubleValue(j_object);
}

void setGlobalDoubleArray(jobject object, const char* name, double* number, int length) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), toJdoubleArray(number, &length));
}

double* getGlobalDoubleArray(jobject object, const char* name, int* length) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobjectArray j_objectArray =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	*length = (*javaEnv)->GetArrayLength(javaEnv, j_objectArray);
	return toDoubleArray(j_objectArray, *length);
}

void setGlobalDoubleMatrix(jobject object, const char* name, double** number, int rows, int columns) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), toJdoubleMatrix(number, &rows, &columns));
}

double** getGlobalDoubleMatrix(jobject object, const char* name, int* rows, int* columns) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobjectArray j_objectArray =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	*rows = (*javaEnv)->GetArrayLength(javaEnv, j_objectArray);
	*columns = (*javaEnv)->GetArrayLength(javaEnv, (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, j_objectArray, 0));
	return toDoubleMatrix(j_objectArray, *rows, *columns);
}

void setGlobalFloat(jobject object, const char* name, float number) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), newFloatObject(&number));
}

float getGlobalFloat(jobject object, const char* name) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobject j_object =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	return getFloatValue(j_object);
}

void setGlobalFloatArray(jobject object, const char* name, float* number, int length) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), toJfloatArray(number, &length));
}

float* getGlobalFloatArray(jobject object, const char* name, int* length) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobjectArray j_objectArray =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	*length = (*javaEnv)->GetArrayLength(javaEnv, j_objectArray);
	return toFloatArray(j_objectArray, *length);
}

void setGlobalFloatMatrix(jobject object, const char* name, float** number, int rows, int columns) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), toJfloatMatrix(number, &rows, &columns));
}

float** getGlobalFloatMatrix(jobject object, const char* name, int* rows, int* columns) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobjectArray j_objectArray =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	*rows = (*javaEnv)->GetArrayLength(javaEnv, j_objectArray);
	*columns = (*javaEnv)->GetArrayLength(javaEnv, (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, j_objectArray, 0));
	return toFloatMatrix(j_objectArray, *rows, *columns);
}

void setGlobalBool(jobject object, const char* name, bool number) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), newBooleanObject(&number));
}

bool getGlobalBool(jobject object, const char* name) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobject j_object =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	return getBooleanValue(j_object);
}

void setGlobalBoolArray(jobject object, const char* name, bool* number, int length) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), toJbooleanArray(number, &length));
}

bool* getGlobalBoolArray(jobject object, const char* name, int* length) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobjectArray j_objectArray =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	*length = (*javaEnv)->GetArrayLength(javaEnv, j_objectArray);
	return toBoolArray(j_objectArray, *length);
}

void setGlobalBoolMatrix(jobject object, const char* name, bool** number, int rows, int columns) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), toJbooleanMatrix(number, &rows, &columns));
}

bool** getGlobalBoolMatrix(jobject object, const char* name, int* rows, int* columns) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobjectArray j_objectArray =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	*rows = (*javaEnv)->GetArrayLength(javaEnv, j_objectArray);
	*columns = (*javaEnv)->GetArrayLength(javaEnv, (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, j_objectArray, 0));
	return toBoolMatrix(j_objectArray, *rows, *columns);
}

void setGlobalChar(jobject object, const char* name, char number) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), newCharacterObject(&number));
}

char getGlobalChar(jobject object, const char* name) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobject j_object =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	return getCharacterValue(j_object);
}

void setGlobalCharArray(jobject object, const char* name, char* number, int length) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), toJcharArray(number, &length));
}

char* getGlobalCharArray(jobject object, const char* name, int* length) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobjectArray j_objectArray =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	*length = (*javaEnv)->GetArrayLength(javaEnv, j_objectArray);
	return toCharArray(j_objectArray, *length);
}

void setGlobalCharMatrix(jobject object, const char* name, char** number, int rows, int columns) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), toJcharMatrix(number, &rows, &columns));
}

char** getGlobalCharMatrix(jobject object, const char* name, int* rows, int* columns) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobjectArray j_objectArray =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	*rows = (*javaEnv)->GetArrayLength(javaEnv, j_objectArray);
	*columns = (*javaEnv)->GetArrayLength(javaEnv, (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, j_objectArray, 0));
	return toCharMatrix(j_objectArray, *rows, *columns);
}

void setGlobalString(jobject object, const char* name, const char* number) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), newStringObject(&number));
}

const char* getGlobalString(jobject object, const char* name) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobject j_object =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	return getStringValue(j_object);
}

void setGlobalStringArray(jobject object, const char* name, const char** number, int length) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), toJstringArray(number, &length));
}

const char** getGlobalStringArray(jobject object, const char* name, int* length) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobjectArray j_objectArray =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	*length = (*javaEnv)->GetArrayLength(javaEnv, j_objectArray);
	return toStringArray(j_objectArray, *length);
}

void setGlobalStringMatrix(jobject object, const char* name, const char*** number, int rows, int columns) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), toJstringMatrix(number, &rows, &columns));
}

const char*** getGlobalStringMatrix(jobject object, const char* name, int* rows, int* columns) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobjectArray j_objectArray =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	*rows = (*javaEnv)->GetArrayLength(javaEnv, j_objectArray);
	*columns = (*javaEnv)->GetArrayLength(javaEnv, (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, j_objectArray, 0));
	return toStringMatrix(j_objectArray, *rows, *columns);
}

#endif
